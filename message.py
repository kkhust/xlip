'''
Created on May 8, 2011

@author: yuxiangchen
'''

MESSAGE_ERROR = 0 # for printing info when error occurs
MESSAGE_INFO = 1 # for general purpose of transmitting messages
MESSAGE_CONTROL = 2 # for controlling the behaviors of one end

_MESSAGE_HEADER_LOGO = 'XLIPheader'
_MESSAGE_HEADER_MAX_LENGTH = '01234567890' # the maximal message length
_MESSAGE_HEADER = _MESSAGE_HEADER_LOGO + _MESSAGE_HEADER_MAX_LENGTH

class SocketMessage:
    """Low level socket message.
    """
    def __init__(self, type=MESSAGE_INFO, content=''):
        self.type = type
        self.content = content
    
    def __len__(self):
        return len(self.content)

def get_message(obj):
    """Get low level socket message.
    """
    if obj is None:
        raise Exception("Socket is not built yet!")
    
    # get the header
    header = ''
    while len(header) < len(_MESSAGE_HEADER):
        chunk = obj.recv(len(_MESSAGE_HEADER)-len(header))
        if chunk == '':
            raise RuntimeError("Socket connection broken! Header.")
        header = header + chunk
    
    # validate the header
    if len(header) != len(_MESSAGE_HEADER) or header[:len(_MESSAGE_HEADER_LOGO)] != _MESSAGE_HEADER[:len(_MESSAGE_HEADER_LOGO)]:
        msg = SocketMessage(MESSAGE_ERROR, "Wrong message header!")
        send_message(obj, msg)
        return None
    
    # get the message type and content length out of the header
    t = int(header[len(_MESSAGE_HEADER_LOGO)])
    
    l = int(header[len(_MESSAGE_HEADER_LOGO)+1:])
    
    # get the actual content
    content = ''
    while len(content) < l:
        chunk = obj.recv(l-len(content))
        if chunk == '':
            raise RuntimeError("Socket connection broken! Content.")
        content = content + chunk
    
    # construct the message and return
    msg = SocketMessage(t, content)
    return msg

def send_message(obj, msg):
    """Send low level socket message.
    """
    if obj is None:
        raise Exception("Socket is not built yet!")
    
    l = len(msg)
    if len(str(l)) > len(_MESSAGE_HEADER[len(_MESSAGE_HEADER_LOGO)+1:]):
        return False
    else:
        length_str = (len(_MESSAGE_HEADER[len(_MESSAGE_HEADER_LOGO)+1:])-len(str(l)))*'0'+str(l)
    header_str = _MESSAGE_HEADER[:len(_MESSAGE_HEADER_LOGO)]+str(msg.type)+length_str
    to_be_sent = header_str+msg.content
    
    total_sent = 0
    while total_sent < len(to_be_sent):
        sent = obj.send(to_be_sent[total_sent:])
        if sent == 0:
            raise RuntimeError("Socket connection broken! Sent.")
        total_sent = total_sent + sent
    
    return True

############################################

# here defines all the clip types!
ClipTypes = ['Text', 'Img', 'File', 'Video', 'Sound', 'Misc']

class ClipMessage:
    """High level clip message.
    """
    def __init__(self, type='Text', content='', timestamp='', deviceID=''):
        if not type in ClipTypes:
            raise Exception("Clip type is wrong: "+str(type))
        self.type = type
        self.content = content
        self.timestamp = timestamp
        self.deviceID = deviceID

    def __str__(self):
        return self.toXMLStr()

    def toXMLStr(self):
        """Here defines the clip message format. Example:
        <XClipMsg Type='Text' Timestamp='' DeviceID=''>
          <Content String='...'/>
        </XClipMsg>
        """
        return "<XClipMsg Type='%s' Timestamp='%s' DeviceID='%s'>\n<Content String='%s'/>\n</XClipMsg>" % (self.type, self.timestamp, self.deviceID, self.content)

    def fromXMLStr(self, string):
        from xml.dom import minidom
        try:
            xmldoc = minidom.parseString(string)
        except:
            raise Exception('Clip message format is invalid: ' + str(string))

        root = xmldoc.getElementsByTagName('XClipMsg')[0]
        self.type = root.getAttribute('Type')
        self.timestamp = root.getAttribute('Timestamp')
        self.deviceID = root.getAttribute('DeviceID')
        self.content = root.getElementsByTagName('Content')[0].getAttribute('String')


def get_clip(socket_obj):
    """Get clip message from the server.
    """
    raw_content = get_message(socket_obj)
    clip_msg = ClipMessage()
    clip_msg.fromXMLStr(raw_content) # reconstruct the clip message from the raw data
    return clip_msg

def send_clip(socket_obj, clip_msg):
    """Send clip message to the server.
    """
    msg = SocketMessage(content=clip_msg.toXMLStr())
    send_message(socket_obj, msg)
