#!/usr/bin/env python

'''
Created on May 8, 2011

@author: yuxiangchen
'''

from message import *

class XLIPClient:
    def __init__(self, host='127.0.0.1', port=80008):
        self.host = host
        self.port = port
        self.sock = None
        self.observers = []
    
    def set_log_output(self, out):
        self.outor = out
        self.outor('client output')
    
    def add_an_observer(self, observer):
        self.observers.append(observer)
    
    def add_observers(self, observers):
        self.observers.extend(observers)
        
    def clear_observers(self):
        self.observers = []
    
    def update_all_observers_with_string(self, message):
        for observer in self.observers:
            observer(message)
    
    def build_connection(self, host, port):
        import socket
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.connect((host, port))
        except socket.error:
            raise Exception("Could not open socket!")
    
    def close(self):
        self.sock.close()
        self.sock = None
        
    def login(self):
        if self.sock is not None:
            print 'The connection has already been built!'
            return
        self.build_connection(self.host, self.port)
        if self.sock is None:
            print 'The connection to the server is not built yet!'
            return

        # create host listener thread
        import thread
        try:
            thread.start_new_thread(self.host_listener, ())
        except thread.error as e:
            self.close()
            print e
    
    def logout(self):
        if self.sock is None:
            return
        # send_message(self.sock, SocketMessage(MESSAGE_CONTROL, 'close'))
        self.close()

    def send(self, content):
        send_message(self.sock, SocketMessage(content=content))

    def get(self):
        msg = get_message(self.sock)
        if self.outor:
            self.outor(msg.content)
        self.update_all_observers_with_string(msg.content)
        print msg.content

    def host_listener(self):
        try:
            while 1:
                self.get() # get & print
            
            self.close()
            return
        except RuntimeError as e:
            print e
            self.close()
            return
