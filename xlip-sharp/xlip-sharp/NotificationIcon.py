﻿import clr

# 
# * Created by SharpDevelop.
# * User: hulihp
# * Date: 9/8/2012
# * Time: 02:36
# *
# * To change this template use Tools | Options | Coding | Edit Standard Headers.
# 
clr.AddReference('System.Windows.Forms')
#clr.AddReference('System.Core')
clr.AddReference('mscorlib')
import System.Drawing
import System.Windows.Forms
import sys
import time

from System import *
from System.Diagnostics import *
from System.Drawing import *
from System.Threading import *
from System.Windows.Forms import *

from server import * 

from System.Threading import *
import WinForm
import System.Threading


#>>> from System.Threading import Thread, ThreadStart
#>>> def f():
#...     try:
#...         print 'Started'
#...     finally:
#...         Thread.Sleep(5000)
#...         print 'Finished'
#...
#>>> t = Thread(ThreadStart(f))
#>>> t.Start()
#
#>>> t.Abort()



class NotificationIcon(object):
  def __init__(self):
    self.form = None
    self.notifyIcon = NotifyIcon()
#    xlip-sharp
#    resources = System.Resources.ResourceManager("xlip-sharp.NotificationIcon", System.Reflection.Assembly.GetEntryAssembly())
#    self.notifyIcon.Icon = resources.GetObject("xlip.Icon")
    directoryName = System.IO.Path.GetDirectoryName(__file__) 
    fileName = System.IO.Path.Combine(directoryName, "xlip-icon.png") 
    self.notifyIcon.Icon = Icon.FromHandle(System.Drawing.Bitmap(fileName).GetHicon())
    self.thread = None
    self.xlipServer = None
    self.uiContext = None
    self.notifyIcon.ContextMenu = self.InitializeMenu()
    self.notifyIcon.DoubleClick += self.IconDoubleClick

  def InitializeMenu(self):
    self.form = WinForm.WinForm()
    self.uiContext = System.Threading.SynchronizationContext.Current;
    
    contextMenu = ContextMenu()
    
    exitMenuItem2 = MenuItem("Start server")
    exitMenuItem2.Click += self.menuStartServerClick
    contextMenu.MenuItems.Add(exitMenuItem2)
    
    exitMenuItem = MenuItem("Exit")
    exitMenuItem.Click += self.menuExitClick
    contextMenu.MenuItems.Add(exitMenuItem)
    
    exitMenuItem1 = MenuItem("About")
    exitMenuItem1.Click += self.menuAboutClick
    contextMenu.MenuItems.Add(exitMenuItem1)
    
    return contextMenu
#    menu = Array[MenuItem]((MenuItem("About", menuAboutClick), MenuItem("Exit", menuExitClick)))
#    return menu
#
#  def Main(args): # <summary>Program entry point.</summary>
#    # <param name="args">Command Line Arguments</param>
#    pass
#  
#  Main = staticmethod(Main)

  # The application is already running
  # TODO: Display message box or change focus to existing application instance # releases the Mutex
  def menuAboutClick(self, sender, e):
    MessageBox.Show("Xlip\n===========\nA cross-platform clipboard management software.")

  def menuExitClick(self, sender, e):
    if self.thread :
      if self.xlipServer:
        self.xlipServer.shut_down()
        MessageBox.Show("self.xlipServer.shut_down()")
      self.thread.Abort()
    self.form.DoClose()
    Application.Exit()
    
  def  menuStartServerClick(self, sender, e):
    if not self.thread :
      self.thread = Thread(ParameterizedThreadStart(self.startServer))
      self.thread.IsBackground = True;
      if not self.thread.IsAlive :
#    if self.uiContext:
        self.thread.Start(self.uiContext)

    
  def setFormTextBox(self, text):
    if self.form:
      self.form.setText(text)
      
  def setFormTextBox2(self, text):
    2+2
  
  def startServer(self, state):
    import socket
    host = socket.gethostbyname(socket.gethostname())
    port = 11088
    # create socket server
    self.xlipServer = None
    try:
      self.xlipServer  = XLIPServer(host, port)
      self.xlipServer.setup()
      MessageBox.Show("self.xlipServer.setup")
      self.xlipServer.add_receive_observer(self.setFormTextBox)
      MessageBox.Show("self.xlipServer.add_receive_observer")
#      self.xlipServer.add_begin_accepting_events(self.ShowWindow)
    except Exception as e:
      MessageBox.Show("print >> sys.__stderr__, e")
      print >> sys.__stderr__, e
#      sys.exit(1)
    self.xlipServer.accept()
    
  def IconDoubleClick(self, sender, e):
#    MessageBox.Show("The icon was double clicked")
    # TODO show the app window.
    if  self.form.Visible :
      self.form.Hide()
    else:
      self.form.Show()
  
  def ShowWindow(self):
    if  self.form :
      self.form.Show()
