#!/usr/bin/env python

'''
Created on Sep 6, 2012

@author: yuxiangchen
'''

import sys
import time
from _struct import *

class XLIPServerLog:
    """Serves as the log of the server.
    """
    def __init__(self, out):
        self.out = out
    
    def add(self, log):
        """Add new log.
        """
        print >> self.out, '[%s]' % time.ctime(), str(log)
        self.out.flush()
    
    def close(self):
        self.out.close()

class XLIPServer:
    def __init__(self, host='', port=80008, max_clients=1):
        self.host = host
        self.port = port
        self.max_clients = max_clients
        
        self.sock = None
        self.threads = []
        self.receive_observers = []
        self.on_begin_accepting_events = []
    
    def setup(self):
        import socket
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.bind((self.host, self.port))
            self.sock.listen(self.max_clients)
        except socket.error:
            raise Exception("Could not open socket!")
    
    def accept(self):
        for observer in self.receive_observers:
          observer('server:' + str(self.host) +  ' is accepting at port:' + str(self.port))
        for callback in self.on_begin_accepting_events:
          callback()
        import thread
        while 1:
            conn, addr = self.sock.accept()
            print 'Connected by %s' % str(addr)
#            conn.send('Hello by Xlip!')
            dataLength = unpack("H", conn.recv(2))[0]
            text = conn.recv(dataLength)
#            text = conn.recv(10)
            for observer in self.receive_observers:
              observer("received:\n"+text)
            toSend = text + "------ read and send back by server."
#            conn.sendall("")
            conn.sendall(pack("H",len(toSend)))
            conn.send(toSend)
    
    def add_receive_observer(self, observer):
      self.receive_observers.append(observer)
    
    def add_begin_accepting_events(self, callback):
      self.on_begin_accepting_events.append(callback)
    
    def close_socket(self):
        self.sock.close()
    
    def shut_down(self):
        # firstly close the socket
        self.close_socket()
        
        # don't forget to close the backend and out

if __name__ == '__main__':
    # parser the command line arguments
    import sys, getopt
    
    usage = 'server.py -h hostname -p port'
    
    if len(sys.argv) == 1:
        print "No argument is given!"
        print usage
        sys.exit(1)
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h:p:", ["help"])
    except getopt.GetoptError:
        print 'Command not right. Exit!'
        sys.exit()
    
    for o,a in opts:
        if o == "--help":
            print usage
            sys.exit()
        if o == "-h":
            hostname = a
        if o == "-p":
            port = int(a)
    
    # create socket server
    try:
        s = XLIPServer(hostname, port)
        s.setup()
    except Exception as e:
        print >> sys.__stderr__, e
        sys.exit(1)
    
    s.accept()
    
    s.shut_down()
    