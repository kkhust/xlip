﻿
import System.Drawing
import System.Windows.Forms

from System.Drawing import *
from System.Windows.Forms import *

class WinForm(Form):
  def __init__(self):
    self.InitializeComponent()
    self._close = False
  
  def InitializeComponent(self):
    self._textBox1 = System.Windows.Forms.TextBox()
    self.SuspendLayout()
    # 
    # textBox1
    # 
    self._textBox1.Location = System.Drawing.Point(12, 12)
    self._textBox1.Multiline = True
    self._textBox1.Name = "textBox1"
    self._textBox1.ReadOnly = True
    self._textBox1.Size = System.Drawing.Size(260, 238)
    self._textBox1.TabIndex = 0
    # 
    # WinForm
    # 
    self.ClientSize = System.Drawing.Size(284, 262)
    self.Controls.Add(self._textBox1)
    self.MaximizeBox = False
    self.MinimizeBox = False
    self.Name = "WinForm"
    self.Text = "WinForm"
    self.FormClosing += self.Cancel_FormClosing
    self.ResumeLayout(False)
    self.PerformLayout()
  
  def Cancel_FormClosing(self, sender, e):
    if not self._close :
      self.Hide()
      e.Cancel = True
      
  def DoClose(self):
    self._close = True
    
    
#  def InvokeIfRequired(f):
#    def wrap(*args):
#      if args[0].InvokeRequired:
#        return args[0].Invoke(CallTarget0(lambda: f(*args)))
#      else:
#        return f(*args)
#    return wrap
#
#  @InvokeIfRequired
  def setText(self, text):
    self._textBox1.Text = text

