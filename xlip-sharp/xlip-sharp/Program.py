﻿import clr
clr.AddReference('System.Windows.Forms')
clr.AddReference('System.Drawing')

import System.Windows.Forms

from System import *
from System.Diagnostics import *
from System.Threading import *
from System.Windows.Forms import *

import NotificationIcon
import MessageForm


  
Application.EnableVisualStyles()
Application.SetCompatibleTextRenderingDefault(False)
# Please use a unique name for the mutex to prevent conflicts with other programs
from System.Threading import Mutex
m = Mutex(False, "xlip-sharp")
if m.WaitOne(0, False):
  notificationIcon = NotificationIcon.NotificationIcon()
  notificationIcon.notifyIcon.Visible = True
  Application.Run()
  notificationIcon.notifyIcon.Dispose()
else :
  Application.Run(MessageForm.MessageForm())