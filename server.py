#!/usr/bin/env python

'''
Created on Sep 6, 2012

@author: yuxiangchen
'''

import sys
import time
from message import *

class XLIPServerLog:
    """Serves as the log of the server.
    """
    def __init__(self, out):
        self.out = out
    
    def add(self, log):
        """Add new log.
        """
        print >> self.out, '[%s]' % time.ctime(), str(log)
        self.out.flush()
    
    def close(self):
        self.out.close()

class XLIPServer:
    def __init__(self, host='', port=80008, max_clients=1):
        self.host = host
        self.port = port
        self.max_clients = max_clients
        
        self.sock = None
        self.threads = []
        self.connections = []
    
    def set_log_output(self, out):
        self.outor = out
        self.outor('server output')

    def setup(self):
        import socket
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.sock.bind((self.host, self.port))
            self.sock.listen(self.max_clients)
        except socket.error:
            raise Exception("Could not open socket!")
    
    def accept(self):
        """Accept the incoming connections
        """
        import thread
        print "waiting for connection"
        self.outor( 'waiting for connection' )
        while 1:
            conn, addr = self.sock.accept()
            print 'Connected by %s' % str(addr)
            self.outor( 'Connected by ' )
            self.outor( 'Connected by %s' % str(addr) )
            try:
                self.threads.append(thread.start_new_thread(self.client_thread, (conn, addr)))
                self.connections.append((conn, addr))
            except thread.error as e:
                conn.close()
                print e
                  
    def accept_on_threads(self):
        self.outor( 'accept_on_threads' )
        import thread
        self.threads.append(thread.start_new_thread(self.accept,()))
    
    def close_socket(self):
        self.sock.close()
    
    def shut_down(self):
        # firstly close the socket
        self.close_socket()
        
        # don't forget to close the backend and out

    def client_thread(self, conn, addr):
        try:
            # authentication
            
            # ready for processing incoming message
            while 1:
                msg = get_message(conn)
                self.outor( '%s received by server from %s' % ( msg.content, str(addr)) )
                self.broadcast_msg(msg)
            
            conn.close()
            return
        except Exception as e:
            conn.close()
            print e
            return

    def broadcast_msg(self, msg):
        for conn, addr in self.connections:
            # print 'Broadcasting message to: ' + str(conn)
            send_message(conn, msg)

if __name__ == '__main__':
    # parser the command line arguments
    import sys, getopt
    
    usage = 'server.py -h hostname -p port'
    
    if len(sys.argv) == 1:
        print "No argument is given!"
        print usage
        sys.exit(1)
    
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h:p:", ["help"])
    except getopt.GetoptError:
        print 'Command not right. Exit!'
        sys.exit()
    
    for o,a in opts:
        if o == "--help":
            print usage
            sys.exit()
        if o == "-h":
            hostname = a
        if o == "-p":
            port = int(a)
    
    # create socket server
    try:
        s = XLIPServer(hostname, port)
        s.setup()
    except Exception as e:
        print >> sys.__stderr__, e
        sys.exit(1)
    
    s.accept()
    
    s.shut_down()
    