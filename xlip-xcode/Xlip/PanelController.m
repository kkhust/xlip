#import "PanelController.h"
#import "BackgroundView.h"
#import "StatusItemView.h"
#import "MenubarController.h"
#import "AppDelegate.h"

#define OPEN_DURATION .15
#define CLOSE_DURATION .1

#define SEARCH_INSET 17

#define POPUP_HEIGHT 60
#define PANEL_WIDTH 320
#define MENU_ANIMATION_DURATION .05

#pragma mark -

@interface NSTextField (copypaste)

-(BOOL)performKeyEquivalent:(NSEvent *)theEvent;
@end
@implementation NSTextField (copypaste)

-(BOOL)performKeyEquivalent:(NSEvent *)theEvent
{
    if ( NSCommandKeyMask == ([theEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask)) {
        if ( [[theEvent charactersIgnoringModifiers] isEqualToString:@"x"]) {
            return [NSApp sendAction:@selector(cut:) to:[[self window] firstResponder] from:self];
        }
        else if ( [[theEvent charactersIgnoringModifiers] isEqualToString:@"c"]) {
            return [NSApp sendAction:@selector(copy:) to:[[self window] firstResponder] from:self];
        }
        else if ( [[theEvent charactersIgnoringModifiers] isEqualToString:@"v"]) {
            return [NSApp sendAction:@selector(paste:) to:[[self window] firstResponder] from:self];
        }
        else if ( [[theEvent charactersIgnoringModifiers] isEqualToString:@"a"]) {
            return [NSApp sendAction:@selector(selectAll:) to:[[self window] firstResponder] from:self];
        }
    }
    return [super performKeyEquivalent:theEvent];
}

@end



@interface NSTextView (copypaste)

-(BOOL)performKeyEquivalent:(NSEvent *)theEvent;
@end
@implementation NSTextView (copypaste)

-(BOOL)performKeyEquivalent:(NSEvent *)theEvent
{
    if ( NSCommandKeyMask == ([theEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask)) {

        if ( [[theEvent charactersIgnoringModifiers] isEqualToString:@"c"]) {
            return [NSApp sendAction:@selector(copy:) to:[[self window] firstResponder] from:self];
        }
        else if ( [[theEvent charactersIgnoringModifiers] isEqualToString:@"a"]) {
            return [NSApp sendAction:@selector(selectAll:) to:[[self window] firstResponder] from:self];
        }
    }
    return [super performKeyEquivalent:theEvent];
}

@end

@implementation PanelController
@synthesize hostField = _hostField;

@synthesize connectButton = _connectButton;
@synthesize backgroundView = _backgroundView;
@synthesize statusItemView = _statusItemView;


- (PanelController *)initWithStatusItemView:(StatusItemView *)statusItemView
{
    self = [super initWithWindowNibName:@"Panel"];
    if (self != nil)
    {
        _statusItemView = statusItemView;
    }
    return self;
}

#pragma mark -


- (void)dealloc
{
    [super dealloc];
}

#pragma mark -

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    // Make a fully skinned panel
    NSPanel *panel = (id)[self window];
    [panel setAcceptsMouseMovedEvents:YES];
    [panel setLevel:NSPopUpMenuWindowLevel];
    [panel setOpaque:NO];
    [panel setBackgroundColor:[NSColor clearColor]];
    
    // Resize panel
    NSRect panelRect = [[self window] frame];
    panelRect.size.height = POPUP_HEIGHT;
    [[self window] setFrame:panelRect display:NO];
    
}


//- (void) mouseDown:(NSEvent *)theEvent
//{
//    NSLog(@"panel: mouseDown");
//}

- (void) windowDidLoad
{
    self.hostField.delegate = self;
//    self.connectButton 
    [self.connectButton setKeyEquivalent:@"\r"];
}
#pragma mark - Public accessors

- (IBAction)connect:(NSButton *)sender {
    
//    NSLog(@"connect:(NSButton *)sender");
    
//    [self ]
//    [self.hostField ];
//    [sender becomeFirstResponder];
    NSString *hostString = [self.hostField stringValue];
    if ([hostString length] > 0)
    {
        [(AppDelegate *)[[NSApplication sharedApplication] delegate] connect2Server:hostString];
        [self closePanel];
    }
}

//- (BOOL)control:(NSControl*)control textView:(NSTextView*)textView doCommandBySelector:(SEL)commandSelector
//{
//    BOOL result = NO;
//    
//    if (commandSelector == @selector(insertNewline:))
//    {
//        // new line action:
//        // always insert a line-break character and don’t cause the receiver to end editing
////        [textView insertNewlineIgnoringFieldEditor:self];
//        [self.hostField abortEditing];
//        result = YES;
//    }
////    else if (commandSelector == @selector(insertTab:))
////    {
////        // tab action:
////        // always insert a tab character and don’t cause the receiver to end editing
//////        [textView insertTabIgnoringFieldEditor:self];
////        result = YES;
////    }
//    
//    return result;
//}

- (BOOL)hasActivePanel
{
    return _hasActivePanel;
}

- (void)setHasActivePanel:(BOOL)flag
{
    if (_hasActivePanel != flag)
    {
        _hasActivePanel = flag;
        
        if (_hasActivePanel)
        {
            [self openPanel];
        }
        else
        {
            [self closePanel];
        }
    }
}

#pragma mark - NSWindowDelegate

- (void)windowWillClose:(NSNotification *)notification
{
    self.hasActivePanel = NO;
}

- (void)windowDidResignKey:(NSNotification *)notification;
{
    if ([[self window] isVisible])
    {
        self.hasActivePanel = NO;
    }
}
//
//- (void)windowDidResize:(NSNotification *)notification
//{
//    NSWindow *panel = [self window];
//    NSRect statusRect = [self statusRectForWindow:panel];
//    NSRect panelRect = [panel frame];
//    
//    CGFloat statusX = roundf(NSMidX(statusRect));
//    CGFloat panelX = statusX - NSMinX(panelRect);
//    
//    self.backgroundView.arrowX = panelX;
//    
//    NSRect searchRect = [self.hostField frame];
//    searchRect.size.width = NSWidth([self.backgroundView bounds]) - SEARCH_INSET * 2;
//    searchRect.origin.x = SEARCH_INSET;
//    searchRect.origin.y = NSHeight([self.backgroundView bounds]) - ARROW_HEIGHT - SEARCH_INSET - NSHeight(searchRect);
//    
//    if (NSIsEmptyRect(searchRect))
//    {
//        [self.hostField setHidden:YES];
//    }
//    else
//    {
//        [self.hostField setFrame:searchRect];
//        [self.hostField setHidden:NO];
//    }
//    
//}

#pragma mark - Keyboard

- (void)cancelOperation:(id)sender
{
    self.hasActivePanel = NO;
}

#pragma mark - Public methods

- (NSRect)statusRectForWindow:(NSWindow *)window
{
    NSRect screenRect = [[[NSScreen screens] objectAtIndex:0] frame];
    NSRect statusRect = NSZeroRect;
    
    StatusItemView *statusItemView = nil;
    if (self.statusItemView)
    {
        statusItemView = self.statusItemView;
    }
    
    if (statusItemView)
    {
        statusRect = statusItemView.globalRect;
        statusRect.origin.y = NSMinY(statusRect) - NSHeight(statusRect);
    }
    else
    {
        statusRect.size = NSMakeSize(STATUS_ITEM_VIEW_WIDTH, [[NSStatusBar systemStatusBar] thickness]);
        statusRect.origin.x = roundf((NSWidth(screenRect) - NSWidth(statusRect)) / 2);
        statusRect.origin.y = NSHeight(screenRect) - NSHeight(statusRect) * 2;
    }
    return statusRect;
}

- (void)openPanel
{
    NSWindow *panel = [self window];
    
    NSRect screenRect = [[[NSScreen screens] objectAtIndex:0] frame];
    NSRect statusRect = [self statusRectForWindow:panel];

    NSRect panelRect = [panel frame];
    panelRect.size.width = PANEL_WIDTH;
    panelRect.origin.x = roundf(NSMidX(statusRect) - NSWidth(panelRect) / 2);
    panelRect.origin.y = NSMaxY(statusRect) - NSHeight(panelRect);
    
    if (NSMaxX(panelRect) > (NSMaxX(screenRect) - ARROW_HEIGHT))
        panelRect.origin.x -= NSMaxX(panelRect) - (NSMaxX(screenRect) - ARROW_HEIGHT);
    
    [NSApp activateIgnoringOtherApps:NO];
    [panel setAlphaValue:0];
    [panel setFrame:statusRect display:YES];
    [panel makeKeyAndOrderFront:nil];
    
    NSTimeInterval openDuration = OPEN_DURATION;
    
    NSEvent *currentEvent = [NSApp currentEvent];
    if ([currentEvent type] == NSLeftMouseDown)
    {
        NSUInteger clearFlags = ([currentEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask);
        BOOL shiftPressed = (clearFlags == NSShiftKeyMask);
        BOOL shiftOptionPressed = (clearFlags == (NSShiftKeyMask | NSAlternateKeyMask));
        if (shiftPressed || shiftOptionPressed)
        {
            openDuration *= 10;
            
            if (shiftOptionPressed)
                NSLog(@"Icon is at %@\n\tMenu is on screen %@\n\tWill be animated to %@",
                      NSStringFromRect(statusRect), NSStringFromRect(screenRect), NSStringFromRect(panelRect));
        }
    }
    
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:openDuration];
    [[panel animator] setFrame:panelRect display:YES];
    [[panel animator] setAlphaValue:1];
    [NSAnimationContext endGrouping];
    
//    [panel performSelector:@selector(makeFirstResponder:) withObject:self.hostField afterDelay:openDuration];
}

- (void)closePanel
{
    [NSAnimationContext beginGrouping];
    [[NSAnimationContext currentContext] setDuration:CLOSE_DURATION];
    [[[self window] animator] setAlphaValue:0];
    [NSAnimationContext endGrouping];
    
    dispatch_after(dispatch_walltime(NULL, NSEC_PER_SEC * CLOSE_DURATION * 2), dispatch_get_main_queue(), ^{
        
        [self.window orderOut:nil];
    });
}

@end
