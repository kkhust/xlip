#-*- coding: utf-8 -*-
#
#  CocoaPythonTestProjectAppDelegate.py
#  CocoaPythonTestProject
#
#  Created by icehuli on 20/10/2012.
#  Copyright icehuli 2012. All rights reserved.
#

import objc
from Foundation import *
from AppKit import *
from Quartz import *
from server import *
from client import *


MenubarController = objc.lookUpClass('MenubarController')
BackgroundView = objc.lookUpClass('BackgroundView')
PanelController = objc.lookUpClass('PanelController')
StatusItemView = objc.lookUpClass('StatusItemView')

start_time = NSDate.date()

class AppDelegate(NSObject):
    
    state = 'idle'
    logWindow = objc.IBOutlet('logWindow')
    logServerTextView = objc.IBOutlet('logServerTextView')
    logClientTextView = objc.IBOutlet('logClientTextView')
    
#    def init(self):
#        NSLog("__init__")
#        self = super(AppDelegate, self).init()
#        self.client = None
#        self.server = None
##        self.client_observers = []
#        self.address = ''
        
    
    def appLogWindow(self):
        return self.logWindow
    
    def appLogServerTextView(self):
        return self.logServerTextView
    
    def appLogClientTextView(self):
        return self.logClientTextView
    
    def setAppStatusItemView_(self, statusItemView):
        self.statusItemView = statusItemView
    
    def applicationDidFinishLaunching_(self, sender):
        self.address = self.getIPWithNSHost_(0)
        print self.address
        self.client = None
        self.server = None
        if not hasattr(self, 'client_observers'):
            self.client_observers = []
        

        #        self.client_observers = []
        #        self.address = ''
        
        self.logServerTextView.appendWithString_("Local Server:\n")
        self.logClientTextView.appendWithString_("Client:\n")
        NSLog("Application did finish launching.")
    
        self.menubarController = MenubarController.alloc().init()
        self.panelController = PanelController.alloc().initWithStatusItemView_(self.menubarController.statusItemView())
        self.menubarController.setPanelController_(self.panelController)
#        self.panelController.addObserver_forKeyPath_options_context(self, "hasActivePanel", 0, kContextActivePanel);
#        self.initialStatusItem(self.menubarController.statusItem())
#        self.initialTimer_(0.5)
#        self.connect2ServerView = ItemDetailViewContoller.initWithFrame_(self.statusItemGlobalRect())
#        self.connect2ServerView = ItemDetailViewContoller.alloc().initWithStatusRect_(self.statusItemGlobalRect())
    
    
    @objc.IBAction
    def togglePanel_(self, sender):
        self.menubarController.hasActiveIcon_(not self.menubarController.hasActiveIcon())
        self.panelController.hasActivePanel_(self.menubarController.hasActiveIcon())

    
    def initialStatusItem(self):
        self.statusItem = NSStatusBar.systemStatusBar().statusItemWithLength_(NSVariableStatusItemLength)
        bundle = NSBundle.mainBundle()
        statusImage = NSImage.alloc().initWithContentsOfFile_(bundle.pathForResource_ofType_("logo20" ,"png"))
        statusImage.setScalesWhenResized_(True)
        self.statusItem.setImage_(statusImage)
        #make the menu
        self.menubarMenu = NSMenu.alloc().init()
        self.menuItemStartServer = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_('Start Server', 'startServer:', '')
        self.menuItemStartClient = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_('Connect to Server', 'connect2Server:', '')
        self.separatorMenuItem = NSMenuItem.separatorItem()
        self.quit = NSMenuItem.alloc().initWithTitle_action_keyEquivalent_('Quit', 'terminate:', '')
        self.menubarMenu.addItem_(self.menuItemStartServer)
        self.menubarMenu.addItem_(self.menuItemStartClient)
        self.menubarMenu.addItem_(self.separatorMenuItem)
        self.menubarMenu.addItem_(self.quit)
        # Bind it to the status item
        self.statusItem.setMenu_(self.menubarMenu)
    

    def statusItemGlobalRect(self):
#        frame = self.statusItem.view();
#        print type(frame)
#        view = NSView.alloc().init()
#        print type(view)
#        print type(view.frame())
        frame = CGRectMake(0.0, 0.0, 24.0, 24.0)
        frame.origin = self.statusItem.window.convertBaseToScreen_(frame.origin);
        frame.origin.y = NSMinY(frame) - NSHeight(frame)
        return frame
    
    def initialTimer_(self, interval):
        self.timer = NSTimer.alloc().initWithFireDate_interval_target_selector_userInfo_repeats_(
                                                        start_time, interval, self, 'tick:', None, True)
        NSRunLoop.currentRunLoop().addTimer_forMode_(self.timer, NSDefaultRunLoopMode)
        self.timer.fire()
    
    def sync_(self, notification):
        print "sync"
    
    def tick_(self, notification):
#        print self.state
#        self.client.send()
        pass
    
    def serverAddress(self):
        return self.address
    
    def getIPWithNSHost_(self, sender):
        addresses = NSHost.currentHost().addresses()
        for anAddress in addresses:
            if (not anAddress.hasPrefix_("127")) and (anAddress.componentsSeparatedByString_(".").count() == 4):
                stringAddress = anAddress
                break
            else:
                stringAddress = "IPv4 address not available"
        return stringAddress
    
    def startServer_(self, sender):
        NSLog('App startServer_!')
        try:
            self.server = XLIPServer(self.address, 10086, 2)
            self.server.set_log_output(self.server_output)
            self.server.setup()
        except Exception as e:
            print >> sys.__stderr__, e
            sys.exit(1)
        self.server.accept_on_threads()
    
    def stopServer_(self, sender):
        NSLog('App stopServer_!')
        try:
            self.server.shut_down()
        except Exception as e:
            print >> sys.__stderr__, e
            sys.exit(1)

    def connect2Server_(self, hostString):
        NSLog('App connect2Server_!')
        print "client"
        self.address = hostString
        self.client = XLIPClient(self.address, 10086)
        self.client.set_log_output(self.client_output)
        self.client_observers.append(self.update_paste_board)
        self.client.add_observers(self.client_observers)
        self.client.login()
        if hasattr(self, 'statusItemView'):
            self.statusItemView.startListenPasteBoard()

    def send_(self, message):
        if self.client:
            self.client.send(message)
        
    def server_output(self, message):
        NSLog(message)
        self.logServerTextView.appendWithString_('%s\n' % message)
    
    def client_output(self, message):
        NSLog(message)
        self.logClientTextView.appendWithString_('%s\n' % message)
    
    def addClientObserver_(self, observer):
        if not hasattr(self, 'client_observers'):
            self.client_observers = []
        self.client_observers.append(observer)

    def update_paste_board(self, message):
        if hasattr(self, 'statusItemView'):
            self.statusItemView.updataPasteBoardWithString_(message)
        
