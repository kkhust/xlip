#import "BackgroundView.h"
#import "StatusItemView.h"



#pragma mark -

@interface PanelController : NSWindowController <NSWindowDelegate, NSTextFieldDelegate>
{
    BOOL _hasActivePanel;
    __unsafe_unretained BackgroundView *_backgroundView;
    __unsafe_unretained NSSearchField *_searchField;
    __unsafe_unretained NSTextField *_textField;
}

@property (nonatomic, unsafe_unretained) IBOutlet BackgroundView *backgroundView;
//@property (nonatomic, unsafe_unretained) IBOutlet NSSearchField *searchField;
//@property (nonatomic, unsafe_unretained) IBOutlet NSTextField *textField;
//@property (nonatomic, assign) IBOutlet NSTextField *hostField;
@property (assign) IBOutlet NSTextField *hostField;

@property (assign) IBOutlet NSButton *connectButton;
- (IBAction)connect:(NSButton *)sender;

@property (nonatomic) BOOL hasActivePanel;
@property (nonatomic, assign) StatusItemView *statusItemView;


- (PanelController *)initWithStatusItemView:(StatusItemView *)statusItemView;


- (void)openPanel;
- (void)closePanel;

@end
