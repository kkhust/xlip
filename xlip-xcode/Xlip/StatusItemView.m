#import "StatusItemView.h"
#import "AppDelegate.h"
#import "NSTextView+asyncAppendString.h"

@interface StatusItemView()


@property NSTimeInterval duration;
@property (strong) NSTimer *timer;
@property int copyCount;
@property (strong) NSTextField *label;
@property NSInteger lastChangeCount;
@property (assign) NSPasteboard *pasteboard;


@end

@implementation StatusItemView

@synthesize statusItem = _statusItem;
@synthesize image = _image;
@synthesize alternateImage = _alternateImage;
@synthesize isHighlighted = _isHighlighted;
@synthesize action = _action;
@synthesize target = _target;


@synthesize timer = _timer;
@synthesize duration = _duration;
@synthesize copyCount = _copyCount;
@synthesize lastChangeCount = _lastChangeCount;
@synthesize pasteboard = _pasteboard;
@synthesize label = _label;
//@synthesize statusMenu = _statusMenu;

#pragma mark -

//- (StatusItemView *)initWithStatusItem:(NSStatusItem *)statusItem
//{
//    CGFloat itemWidth = [statusItem length];
//    CGFloat itemHeight = [[NSStatusBar systemStatusBar] thickness];
//    NSRect itemRect = NSMakeRect(0.0, 0.0, itemWidth, itemHeight);
//    self = [super initWithFrame:itemRect];
//    
//    if (self != nil) {
//        _statusItem = statusItem;
//        _statusItem.view = self;
////        [_statusItem setMenu:self.statusMenu];
//        NSLog(@"%@",_statusItem);
//    }
//    return self;
//}



- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.duration = 0.5;
        
        self.pasteboard = [NSPasteboard generalPasteboard];
        
        [(AppDelegate *)[[NSApplication sharedApplication] delegate]
         setAppStatusItemView:self];
//         addClientObserver:
//         [self updataPasteBoardWithString]];
//        ^(id anObject) { NSLog(@"Done: %@", anObject); }
        
        NSArray *supportedTypes =
        [NSArray arrayWithObjects:
         //  NSRTFDPboardType, NSRTFPboardType,NSStringPboardType,
         NSColorPboardType,
         NSSoundPboardType,
         NSFontPboardType,
         NSRulerPboardType,
         NSTabularTextPboardType,
         NSMultipleTextSelectionPboardType,
         NSFindPanelSearchOptionsPboardType,
         NSStringPboardType,
         NSPDFPboardType,
         NSRTFPboardType,
         NSRTFDPboardType,
         NSTIFFPboardType,
         NSHTMLPboardType,
         nil];
        //    NSString *bestType = [self.pasteboard availableTypeFromArray:supportedTypes];
        
        //    NSLog(@"%@",[self.pasteboard types]);
        
        
        self.lastChangeCount = [[NSPasteboard generalPasteboard] changeCount];
        self.copyCount = 0;
        
        
        //register for drags
        //    [self registerForDraggedTypes:[NSArray arrayWithObjects:
        ////                                   NSFilenamesPboardType,
        ////                                   NSStringPboardType,
        //                                   NSColorPboardType,
        //                                   NSSoundPboardType,
        //                                   NSFontPboardType,
        //                                   NSRulerPboardType,
        //                                   NSTabularTextPboardType,
        //                                   NSMultipleTextSelectionPboardType,
        //                                   NSFindPanelSearchOptionsPboardType,
        //                                   nil]];
        
        NSBundle *bundle = [NSBundle mainBundle];
        
        NSImage *statusImage = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"logo24" ofType:@"png"]];
        NSImageView *imageView = [[NSImageView alloc] initWithFrame:CGRectMake(frame.origin.x,
                                                                               frame.origin.y,
                                                                               frame.size.width/2,
                                                                               frame.size.height)];
        [imageView setImage:statusImage];
        
        self.label = [[NSTextField alloc] initWithFrame:CGRectMake(frame.origin.x+frame.size.width/2-2,
                                                                   frame.origin.y-4,
                                                                   frame.size.width/2,
                                                                   frame.size.height)];
        [self.label setEditable:NO];
        [self.label setSelectable:NO];
        [self.label setFont:[NSFont fontWithName:@"Arial" size:16]];
        //    [self.label setBackgroundColor:[NSColor colorWithCalibratedRed:0.9 green:0.5 blue:0.5 alpha:0.6]];
        [self.label setBackgroundColor:[NSColor clearColor]];
        [self.label setBordered:NO];
        [self.label setStringValue:[NSString stringWithFormat:@"%2i",self.copyCount]];
        self.label.layer.cornerRadius = 6;
        [self addSubview:imageView];
        [self addSubview:self.label];
        
    }
    return self;
}

- (void)startListenPasteBoard
{
    if (!(self.timer)) {
        self.lastChangeCount = [self.pasteboard changeCount];
        self.timer = [NSTimer timerWithTimeInterval:self.duration target:self selector:@selector(timerExpired:) userInfo:nil repeats:YES];
        //    [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:mode];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSDefaultRunLoopMode];
    }
}

#pragma mark -

- (void)drawRect:(NSRect)dirtyRect
{
//	[self.statusItem drawStatusBarBackgroundInRect:dirtyRect withHighlight:self.isHighlighted];
//    
//    NSImage *icon = self.isHighlighted ? self.alternateImage : self.image;
//    NSSize iconSize = [icon size];
//    NSRect bounds = self.bounds;
//    CGFloat iconX = roundf((NSWidth(bounds) - iconSize.width) / 2);
//    CGFloat iconY = roundf((NSHeight(bounds) - iconSize.height) / 2);
//    NSPoint iconPoint = NSMakePoint(iconX, iconY);
//
//	[icon drawAtPoint:iconPoint fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
}

#pragma mark -
#pragma mark Mouse tracking


- (void)timerExpired:(NSTimer*)theTimer
{
    //  self.copyCount++;
//    NSLog(@"tick");
    //  [self.label setStringValue:[NSString stringWithFormat:@"%i",self.copyCount]];
    if ([self.pasteboard changeCount] != self.lastChangeCount)
    {
        //    if ( [[self.pasteboard types] containsObject:NSMultipleTextSelectionPboardType] ) {
        //      NSArray *files = [self.pasteboard propertyListForType:NSMultipleTextSelectionPboardType];
        //
        //      NSString* myString = [self.pasteboard  stringForType:NSPasteboardTypeMultipleTextSelection];
        //      NSLog(@"NSPasteboardTypeMultipleTextSelection: %@",myString);
        //
        //      self.copyCount++;
        //      [self.label setStringValue:[NSString stringWithFormat:@"%i",self.copyCount]];
        //    }
        //    if ( [[self.pasteboard types] containsObject:NSTabularTextPboardType] ) {
        //      NSArray *files = [self.pasteboard propertyListForType:NSTabularTextPboardType];
        //
        //      NSLog(@"NSTabularTextPboardType: %@",files);
        //      NSString* myString = [self.pasteboard  stringForType:NSPasteboardTypeTabularText];
        //      NSLog(@"NSPasteboardTypeTabularText: %@",myString);
        //
        //      self.copyCount++;
        //      [self.label setStringValue:[NSString stringWithFormat:@"%i",self.copyCount]];
        //    }
        //    if ( [[self.pasteboard types] containsObject:NSRTFPboardType] ) {
        //      NSArray *files = [self.pasteboard propertyListForType:NSRTFPboardType];
        //
        //      NSLog(@"NSRTFPboardType: %@",files);
        //      NSString* myString = [self.pasteboard  stringForType:NSPasteboardTypeRTF];
        //      NSLog(@"NSPasteboardTypeRTF: %@",myString);
        //
        //      self.copyCount++;
        //      [self.label setStringValue:[NSString stringWithFormat:@"%i",self.copyCount]];
        //    }
        if ( [[self.pasteboard types] containsObject:NSStringPboardType] ) {
            
            NSString* myString = [self.pasteboard  stringForType:NSPasteboardTypeString];
            NSLog(@"NSPasteboardTypeRTF: %@",myString);
            
            self.copyCount++;
            [self.label setStringValue:[NSString stringWithFormat:@"%i",self.copyCount]];
            
            [(AppDelegate *)[[NSApplication sharedApplication] delegate] send:myString];
        }
        
        self.lastChangeCount = [self.pasteboard changeCount];
    }
}

//we want to copy the files
- (NSDragOperation)draggingEntered:(id<NSDraggingInfo>)sender
{
    return NSDragOperationCopy;
}

//perform the drag and log the files that are dropped
- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender
{
    NSPasteboard *pboard;
    NSDragOperation sourceDragMask;
    
    sourceDragMask = [sender draggingSourceOperationMask];
    pboard = [sender draggingPasteboard];
    
    if ( [[pboard types] containsObject:NSFilenamesPboardType] ) {
        NSArray *files = [pboard propertyListForType:NSFilenamesPboardType];
        
        NSLog(@"Files: %@",files);
    }
    else if ( [[pboard types] containsObject:NSStringPboardType] ) {
        NSArray *files = [pboard propertyListForType:NSStringPboardType];
        
        NSLog(@"Files: %@",files);
    }
    return YES;
}


//
- (void)mouseDown:(NSEvent *)theEvent
{
//    [NSApp sendAction:self.action to:self.target from:self];
    [self.statusItem popUpStatusItemMenu:self.statusItem.menu];
}
//
//#pragma mark -
//#pragma mark Accessors
//
//- (void)setHighlighted:(BOOL)newFlag
//{
//    if (_isHighlighted == newFlag) return;
//    _isHighlighted = newFlag;
//    [self setNeedsDisplay:YES];
//}
//
//#pragma mark -
//
//- (void)setImage:(NSImage *)newImage
//{
//    if (_image != newImage) {
//        _image = newImage;
//        [self setNeedsDisplay:YES];
//    }
//}
//
//- (void)setAlternateImage:(NSImage *)newImage
//{
//    if (_alternateImage != newImage) {
//        _alternateImage = newImage;
//        if (self.isHighlighted) {
//            [self setNeedsDisplay:YES];
//        }
//    }
//}s:s:string

- (void)updataPasteBoardWithString:(NSString *)string
{
    //pause timer
    NSDate *pauseState = [NSDate dateWithTimeIntervalSinceNow:0];
    NSDate *previousFireDate = [self.timer fireDate];
    [self.timer setFireDate:[NSDate distantFuture]];
    
    //write to pasteboard
    [self.pasteboard declareTypes:[NSArray arrayWithObject:NSPasteboardTypeString] owner:self];
    [self.pasteboard setString:string forType:NSPasteboardTypeString];
    self.lastChangeCount = [self.pasteboard changeCount];
    NSLog(@"%@ -> has written to PasteBoard.",string);
    [[(AppDelegate *)[[NSApplication sharedApplication] delegate] appLogClientTextView] appendWithString:[NSString stringWithFormat:@"%@ -> has written to PasteBoard.\n",string]];
    
    //resume timer
    float pauseTime = -1*[pauseState timeIntervalSinceNow];
    [self.timer setFireDate:[NSDate dateWithTimeInterval:pauseTime sinceDate:previousFireDate]];
    
}

#pragma mark -

- (NSRect)globalRect
{
    NSRect frame = [self frame];
    frame.origin = [self.window convertBaseToScreen:frame.origin];
    return frame;
}

@end
