//
//  NSTextView+asyncAppendString.m
//  CocoaPythonTestProject
//
//  Created by icehuli on 25/10/2012.
//  Copyright (c) 2012 icehuli. All rights reserved.
//

#import "NSTextView+asyncAppendString.h"

@implementation NSTextView (asyncAppendString)


- (void) appendWithString:(NSString *)string
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [[self textStorage] appendAttributedString:
            [[NSAttributedString alloc] initWithString:
                [NSString stringWithFormat:@"%@",string]]];
        [self scrollRangeToVisible:NSMakeRange([[self string] length], 0)];
    });
}

@end
