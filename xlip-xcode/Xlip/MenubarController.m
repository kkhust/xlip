#import "MenubarController.h"
#import "AppDelegate.h"

@implementation MenubarController

@synthesize statusMenu = _statusMenu;
@synthesize statusItem = _statusItem;
@synthesize statusItemView = _statusItemView;
@synthesize panelController = _panelController;

#pragma mark -

- (id)init
{
    self = [super init];
    if (self != nil)
    {
        // Install status item into the menu bar
        self.statusItem = [[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength];
        
        [self.statusItem setToolTip:@"You do not need this..."];
        
        self.statusItemView = [[StatusItemView alloc] initWithFrame:NSMakeRect(0, 0, STATUS_ITEM_VIEW_WIDTH, STATUS_ITEM_VIEW_WIDTH)];
        self.statusItemView.statusItem = self.statusItem;
        NSLog(@"%@",self.statusItem);
        [self.statusItem setView:self.statusItemView];
        [self initItemMenu];
        
        NSBundle *bundle = [NSBundle mainBundle];
        NSImage *statusImage = [[NSImage alloc] initWithContentsOfFile:[bundle pathForResource:@"logo24" ofType:@"png"]];
        self.statusItem.image = statusImage;
        NSLog(@"logo24");
    }
    return self;
}

- (void)dealloc
{
    [[NSStatusBar systemStatusBar] removeStatusItem:self.statusItem];
    [super dealloc];
}

#pragma mark -
#pragma mark Public accessors

//- (NSStatusItem *)statusItem
//{
//    return self.statusItemView.statusItem;
//}

#pragma mark -
- (void)initItemMenu
{
    self.statusMenu = [[NSMenu alloc]init];
    NSMenuItem *menuItemStartServer = [[NSMenuItem alloc] initWithTitle:@"Start Server" action:@selector(startServer:) keyEquivalent:@""];
    NSMenuItem *menuItemStopServer = [[NSMenuItem alloc] initWithTitle:@"Stop Server" action:@selector(stopServer:) keyEquivalent:@""];
    NSMenuItem *menuItemCopyServerAddress = [[NSMenuItem alloc] initWithTitle:@"Copy Host" action:@selector(copyServerAddress:) keyEquivalent:@""];
    NSMenuItem *menuItemStartClient = [[NSMenuItem alloc] initWithTitle:@"Connect to Server" action:@selector(connect2Server:) keyEquivalent:@""];
    NSMenuItem *menuItemOpenLog = [[NSMenuItem alloc] initWithTitle:@"Log" action:@selector(openLog:) keyEquivalent:@""];
    NSMenuItem *menuItemQuit = [[NSMenuItem alloc] initWithTitle:@"Quit" action:@selector(terminate:) keyEquivalent:@""];

    menuItemStartServer.target = self;
    menuItemStopServer.target = self;
    menuItemCopyServerAddress.target = self;
    menuItemStartClient.target = self;
    menuItemOpenLog.target = self;
    [self.statusMenu addItem:menuItemStartServer];
    [self.statusMenu addItem:menuItemStopServer];
    [self.statusMenu addItem:menuItemCopyServerAddress];
    [self.statusMenu addItem:menuItemStartClient];
    [self.statusMenu addItem:[NSMenuItem separatorItem]];
    [self.statusMenu addItem:menuItemOpenLog];
    [self.statusMenu addItem:[NSMenuItem separatorItem]];
    [self.statusMenu addItem:menuItemQuit];
    [self.statusItem setMenu:self.statusMenu];
    [self.statusMenu setAutoenablesItems:NO];
//    [menuItemStartServer setEnabled:YES];
//    [menuItemStopServer setEnabled:YES];
    [menuItemCopyServerAddress setEnabled:NO];
//    [menuItemStartClient setEnabled:YES];
//    [[self.statusMenu.itemArray objectAtIndex:2] setEnabled:NO];
    
}

- (void)startServer:(id)sender
{
    NSLog(@"menubar startServer");
    
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] startServer:sender];
    
    
    NSString *addressString = [(AppDelegate *)[[NSApplication sharedApplication] delegate] serverAddress];
    [[self.statusMenu.itemArray objectAtIndex:2] setTitleWithMnemonic:[NSString stringWithFormat:@"Copy Host: %@", addressString]];
    [[self.statusMenu.itemArray objectAtIndex:2] setEnabled:YES];
}

- (void)stopServer:(id)sender
{
    NSLog(@"menubar startServer");
    
    [(AppDelegate *)[[NSApplication sharedApplication] delegate] stopServer:sender];
    [[self.statusMenu.itemArray objectAtIndex:2] setEnabled:NO];
}

- (void)connect2Server:(id)sender
{
    
    [self.panelController setHasActivePanel:YES];
    NSLog(@"menubar connect2Server");
//    [(AppDelegate *)[[NSApplication sharedApplication] delegate] connect2Server:sender];
}

- (void)copyServerAddress:(id)sender
{
    NSLog(@"menubar startServer");
    
    NSString *addressString = [(AppDelegate *)[[NSApplication sharedApplication] delegate] serverAddress];
    [[NSPasteboard generalPasteboard] declareTypes:[NSArray arrayWithObject:NSStringPboardType] owner:nil];
    [[NSPasteboard generalPasteboard] setString:addressString forType:NSStringPboardType];
    
//    string
}

- (void)openLog:(id)sender
{

    AppDelegate *app = [[NSApplication sharedApplication] delegate];
    NSWindow *logWindow = [app appLogWindow];
    NSLog(@"%@", logWindow);
//    [logWindow setViewsNeedDisplay:YES];
    if ([logWindow isVisible]){
        [logWindow orderOut:self]; 
    }
    else{
        [logWindow makeKeyAndOrderFront:self];
        [NSApp activateIgnoringOtherApps:YES];
    }
//    [myWindow orderOut:self]; 
//    [(AppDelegate *)[[NSApplication sharedApplication] delegate] logWindow]
}


- (BOOL)hasActiveIcon
{
    return self.statusItemView.isHighlighted;
}

- (void)setHasActiveIcon:(BOOL)flag
{
    self.statusItemView.isHighlighted = flag;
}

@end
