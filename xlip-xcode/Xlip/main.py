#-*- coding: utf-8 -*-
#
#  main.py
#  CocoaPythonTestProject

#  Created by icehuli on 20/10/2012.
#  Copyright icehuli 2012. All rights reserved.
#

#import modules required by application
import objc
import Foundation
import AppKit
import server
import client

from PyObjCTools import AppHelper

# import modules containing classes required to start application and load MainMenu.nib
import AppDelegate

# pass control to AppKit
AppHelper.runEventLoop()
