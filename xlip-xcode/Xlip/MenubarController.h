
#import "StatusItemView.h"
#import "PanelController.h"
#define STATUS_ITEM_VIEW_WIDTH 24.0

#pragma mark -


@interface MenubarController : NSObject

@property (nonatomic, strong) IBOutlet NSMenu *statusMenu;
@property (nonatomic) BOOL hasActiveIcon;
@property (nonatomic, strong) NSStatusItem *statusItem;
@property (nonatomic, strong) StatusItemView *statusItemView;
@property (nonatomic, assign) PanelController *panelController;


- (void)startServer:(id)sender;
- (void)connect2Server:(id)sender;

@end
