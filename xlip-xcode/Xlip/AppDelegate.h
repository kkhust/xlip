
//typedef void (^Observer)(NSString *message);
#import "StatusItemView.h"

@interface AppDelegate : NSObject {
     IBOutlet NSWindow *logWindow;
    IBOutlet NSTextView *logServerTextView;
    IBOutlet NSTextView *logClientTextView;
}
- (void) startServer:(id)sender;
- (void) stopServer:(id)sender;
- (void) send:(NSString *)message;
- (NSString *) serverAddress;
- (void) connect2Server:(id)sender;

//@property (assign) IBOutlet NSWindow *logWindow;

- (NSWindow *) appLogWindow;
- (NSTextView *) appLogServerTextView;
- (NSTextView *) appLogClientTextView;
- (void) setAppStatusItemView:(StatusItemView *)statusItemView;
//- (void)addClientObserver:(Observer)observer;

@end