//
//  NSTextView+asyncAppendString.h
//  CocoaPythonTestProject
//
//  Created by icehuli on 25/10/2012.
//  Copyright (c) 2012 icehuli. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSTextView (asyncAppendString)

- (void) appendWithString:(NSString *)string;

@end
